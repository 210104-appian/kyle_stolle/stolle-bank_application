package dev.stolle.services;

import static org.junit.Assert.assertEquals;

import dev.stolle.models.Account;
import dev.stolle.models.AccountStatus;
import dev.stolle.models.AccountType;

import org.junit.Test;

public class AccountTest {
	
	@Test
	public void testWithdrawalWithNegativeAmount() {
		boolean expected = false;
		Account account = new Account(25,200.00,AccountType.C,AccountStatus.APPROVED);
		double amount = -10.0;
		boolean actual = account.withdrawal(amount);
		assertEquals(expected, actual);
	}
	
	@Test
	public void testWithdrawalWithAmountLargerThanBalance() {
		boolean expected = false;
		Account account = new Account(25,200.00,AccountType.C,AccountStatus.APPROVED);
		double amount = 300.00;
		boolean actual = account.withdrawal(amount);
		assertEquals(expected, actual);
	} 
	
	@Test 
	public void testWithdrawalWithValidAmount() {
		boolean expected = true;
		Account account = new Account(25,200.00,AccountType.C,AccountStatus.APPROVED);
		double amount = 50.00;
		boolean actual = account.withdrawal(amount);
		assertEquals(expected, actual);
	}
	
	@Test
	public void testDepositWithNegativeAmount() {
		boolean expected = false;
		Account account = new Account(25,200.00,AccountType.C,AccountStatus.APPROVED);
		double amount = -10.0;
		boolean actual = account.deposit(amount);
		assertEquals(expected, actual);
	}
	
	@Test 
	public void testDepositWithValidAmount() {
		boolean expected = true;
		Account account = new Account(25,200.00,AccountType.C,AccountStatus.APPROVED);
		double amount = 300.00;
		boolean actual = account.deposit(amount);
		assertEquals(expected, actual);
	}
	
}
