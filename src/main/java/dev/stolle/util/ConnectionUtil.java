package dev.stolle.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionUtil {
	
	private static Connection connection;
	
	public static Connection getConnection() throws SQLException {
		String url = System.getenv("BANK_URL");
		String username = System.getenv("BANK_USER");
		String password = System.getenv("BANK_PASS");
		
		if (connection == null || connection.isClosed()) {
			connection = DriverManager.getConnection(url,username,password);
		}
		
		return connection;
	}

}
