package dev.stolle.models;

import java.util.List;

public class Customer extends User {
	private List<Account> accounts;
	
	public Customer() {
		super();
	}
	
	public Customer(int userId, String userName, String password, String firstName, String lastName, List<Account> accounts) {
		super(userId,userName,password,firstName,lastName);
		setAccounts(accounts);
	}
	
	public Customer(int userId, String userName, String password, String firstName, String lastName) {
		super(userId,userName,password,firstName,lastName);
	}

	public List<Account> getAccounts() {
		return accounts;
	}

	public void setAccounts(List<Account> accounts) {
		this.accounts = accounts;
	}
	
	
}
