package dev.stolle.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import dev.stolle.models.Account;
import dev.stolle.models.AccountStatus;
import dev.stolle.models.AccountType;
import dev.stolle.models.Customer;
import dev.stolle.models.Employee;
import dev.stolle.util.ConnectionUtil;

public class UserDaoImpl {
	
	private static Logger log = Logger.getRootLogger();
	
	public Customer getCustomer(String userName, String password) {
		log.info("Getting User: " + userName + " information.");
		String sql = "SELECT * FROM Users WHERE User_Name = ? AND Password = ?";
		try (Connection conn = ConnectionUtil.getConnection(); PreparedStatement pStatement = conn.prepareStatement(sql);) {
			pStatement.setString(1, userName);
			pStatement.setString(2, password);
			ResultSet rs = pStatement.executeQuery(); 
			// no results returned 
			if(rs.next() == false) {
				log.error("User not found.");
				return null;
			}
			Customer customer = new Customer(rs.getInt("User_Id"),rs.getString("User_Name"),rs.getString("Password"),rs.getString("First_Name"),rs.getString("Last_Name"));
			customer.setAccounts(getCustomerAccounts(customer.getUserId()));
			
			return customer;
		} catch (SQLException ex) {
			log.error("Error has occurred: " + ex.getClass());
			ex.printStackTrace();
		}
		
		return null;
	} 
	
	public List<Account> getCustomerAccounts(int userId){
		
		List<Account> accounts = new ArrayList<Account>();
		log.info("Getting Customer's: " + userId + " Accounts.");
		String sql = "SELECT * FROM Accounts WHERE User_Id = ?";
		try(Connection conn = ConnectionUtil.getConnection(); PreparedStatement pStatement = conn.prepareStatement(sql);){
			pStatement.setInt(1, userId);
			ResultSet rs = pStatement.executeQuery();
			
			while(rs.next()) {
				Account account = new Account();
				account.setAccountNumber(rs.getInt("Account_Id"));
				account.setBalance(rs.getDouble("Balance")); 
				String accountType = rs.getString("Account_Type");
				if (accountType.equals("C")) {
					account.setType(AccountType.C);
				} else {
					account.setType(AccountType.S);
				}
				String status = rs.getString("Status");
				
				if(status.equals("PENDING_APPROVAL")) {
					account.setStatus(AccountStatus.PENDING_APPROVAL);
				} else if (status.equals("APPROVED")) {
					account.setStatus(AccountStatus.APPROVED);
				} else {
					account.setStatus(AccountStatus.REJECTED);
				}
				
				accounts.add(account);
			} 
			
		} catch (SQLException ex) {
			System.out.println("Error: ");
			ex.printStackTrace();
			log.error("Error occurred retrieving user accounts: " + ex.getClass());
		}
		
		return accounts;
	}
	
	public Employee getEmployee(String userName, String password) {
		log.info("Getting User: " + userName + " information.");
		String sql = "SELECT * FROM Users WHERE User_Name = ? AND Password = ?";
		try (Connection conn = ConnectionUtil.getConnection(); PreparedStatement pStatement = conn.prepareStatement(sql);) {
			pStatement.setString(1, userName);
			pStatement.setString(2, password);
			ResultSet rs = pStatement.executeQuery(); 
			// no results returned 
			if(rs.next() == false) {
				log.error("User not found.");
				return null;
			}
			Employee employee = new Employee(rs.getInt("User_Id"),rs.getString("User_Name"),rs.getString("Password"),rs.getString("First_Name"),rs.getString("Last_Name"));
			return employee;
		} catch (SQLException ex) {
			log.error("Error has occurred: " + ex.getClass());
			ex.printStackTrace();
		}
		
		return null;
	}
	
	public Account getCustomerAccount(Customer customer, int accountId) {
		log.info("Getting account: " + accountId + ", for User: " + customer.getUserName());
		Account account = new Account();
		String sql = "SELECT * FROM Accounts WHERE Account_Id = ? AND User_Id = ?";
		try (Connection conn = ConnectionUtil.getConnection(); PreparedStatement pStatement = conn.prepareStatement(sql);){
			pStatement.setInt(1, accountId);
			pStatement.setInt(2, customer.getUserId());
			ResultSet rs = pStatement.executeQuery(); 
			if (rs == null) {
				return null;
			}
			account.setAccountNumber(rs.getInt("Account_Id"));
			account.setBalance(rs.getDouble("Balance"));
			String type = rs.getString("Account_Type");
			if (type.equals("C")) {
				account.setType(AccountType.C);
			}else {
				account.setType(AccountType.S);
			}
			String status = rs.getString("Status");
			if(status.equals("PENDING_APPROVAL")) {
				account.setStatus(AccountStatus.PENDING_APPROVAL);
			} else if (status.equals("APPROVED")) {
				account.setStatus(AccountStatus.APPROVED);
			} else {
				account.setStatus(AccountStatus.REJECTED);
			}
			
		} catch (SQLException ex) {
			log.error("Error has occurred: " + ex.getClass());
			ex.printStackTrace();
		}
		
		return account;
	}
	
	public List<Customer> getAllCustomers() {
		ArrayList<Customer> customers = new ArrayList<Customer>();
		log.info("Getting all Customers.");
		String sql = "SELECT * FROM Users WHERE User_Type = ?";
		try (Connection conn = ConnectionUtil.getConnection(); PreparedStatement pStatement = conn.prepareStatement(sql);) {
			String type = "C";
			pStatement.setString(1, type);
			ResultSet rs = pStatement.executeQuery(); 
			
			while(rs.next()) {
				Customer customer = new Customer();
				customer.setFirstName(rs.getString("First_Name"));
				customer.setLastName("Last_Name");
				customer.setPassword(rs.getString("Password"));
				customer.setUserName(rs.getString("User_Name"));
				customer.setUserId(rs.getInt("User_Id")); 
				customer.setAccounts(getCustomerAccounts(customer.getUserId()));
				customers.add(customer);
			}
			
		} catch (SQLException ex) {
			log.error("Error has occurred retrieving customers: " + ex.getClass());
		}
		
		return customers;
	}

}
