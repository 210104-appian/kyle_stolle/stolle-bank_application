package dev.stolle.services;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.Scanner;

import dev.stolle.daos.TransactionDaoImpl;
import dev.stolle.daos.UserDaoImpl;
import dev.stolle.models.Account;
import dev.stolle.models.AccountStatus;
import dev.stolle.models.AccountType;
import dev.stolle.models.Customer;
import dev.stolle.models.Employee;

public class TransactionService {
	private Scanner scanner = new Scanner(System.in); 
	private TransactionDaoImpl transaction = new TransactionDaoImpl();
	private UserDaoImpl userDao = new UserDaoImpl();
	
	public void EmployeeTransaction(Employee employee) {
		int input = 0;
		do {
			input = employeeMainMenu();
			if (input == 1) {
				// display pending accounts
				Account account = selectPendingAccount(transaction.getPendingAccounts());
				accountAction(account);
			} else if (input == 2) {
				// display customers
				Customer customer = selectCustomer(userDao.getAllCustomers());
				viewCustomerAccounts(userDao.getCustomerAccounts(customer.getUserId()));
			} else if (input == 3) {
				// display transaction log to console.
				displayLogs();
			} else {
				continue;
			}
		} while(input != 4);
		
	}
	
	public void CustomerTransaction(Customer customer) {
		// prompt user
		int input = 0;
		do {
			input = customerMainMenu();
			if (input == 1) {
				// display accounts 
				Account account = selectAccount(customer);
				withdrawal(account);
				transaction.updateBalance(account);
			} else if (input == 2) {
				Account account = selectAccount(customer);
				deposit(account);
				transaction.updateBalance(account);
			} else if (input == 3){
				// create new account in database
				Account account = applyForAccount();
				transaction.applyForAccount(customer, account);
				customer.setAccounts(userDao.getCustomerAccounts(customer.getUserId()));
			} else if (input == 4) {
				Account account = selectAccount(customer);
				System.out.println(account.toString());
			} else {
				continue;
			}
		} while(input != 5);
		
	}
	
	private void displayLogs() {
		try (FileReader fr = new FileReader("src/main/java/log.txt"); BufferedReader br = new BufferedReader(fr);){
			String line;
			while((line = br.readLine()) != null) {
				String substr = line.substring(20, 24); 
				if(substr.equals("INFO")) {
					System.out.println(line);
				}
			}
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		
		
	}
	
	private void accountAction(Account account) {
		System.out.println("Do you want to (1) approve or (2) reject this account.");
		System.out.println(account.toString());
		int input = Integer.parseInt(scanner.next());
		if (input == 1) {
			transaction.approveAccount(account);
		} else {
			transaction.rejectAccount(account);
		} 
	} 
	
	private void viewCustomerAccounts(List<Account> accounts) {
		for(Account account: accounts) {
			System.out.println(account.toString());
		}
	}
	
	private Customer selectCustomer(List<Customer> customers) {
		System.out.println("Please select a customer who's accounts you would like to view.");
		for(int i = 0; i < customers.size(); i++) {
			System.out.println("(" + i+") " + customers.get(i).toString());
		} 
		
		return customers.get(Integer.parseInt(scanner.next()));
	}
	
	private Account selectPendingAccount(List<Account> accounts) {
		System.out.println("Please Select an account you wish to View");
		for(int i = 0; i < accounts.size(); i++) {
			System.out.println("(" + i + ") Account_Id: " + accounts.get(i).getAccountNumber());
		} 
		
		int input = Integer.parseInt(scanner.next());
		return accounts.get(input);
	}
	
	private int customerMainMenu() {
		System.out.println("Please select the following options.");
		System.out.println("(1) Withdrawal.");
		System.out.println("(2) Deposit.");
		System.out.println("(3) Apply for new account.");
		System.out.println("(4) View Account information.");
		System.out.println("(5) exit.");
		int input = Integer.parseInt(scanner.next());
		return input;
	}
	
	private int employeeMainMenu() {
		System.out.println("Please select the following options.");
		System.out.println("(1) View Pending Accounts.");
		System.out.println("(2) View Customers.");
		System.out.println("(3) View Transaction Log.");
		System.out.println("(4) exit.");
		int input = Integer.parseInt(scanner.next());
		return input;
	}
	
	private Account applyForAccount() {
		Account account = new Account();
		account.setStatus(AccountStatus.PENDING_APPROVAL);
		System.out.println("What type of Account would you like to open?");
		System.out.println("(1) Checking");
		System.out.println("(2) Savings");
		int input = Integer.parseInt(scanner.next());
		if (input == 1) {
			account.setType(AccountType.C);
		} else {
			account.setType(AccountType.C);
		}
		account.setBalance(setStartingBalance());
		return account;
	}
	
	private double setStartingBalance() {
		System.out.println("How much would you like to deposit for a starting balance?");
		double startingBalance = Double.parseDouble(scanner.next());
		return startingBalance;
	}
	
	private void deposit(Account account) {
		System.out.println("Please enter the amount you would like to deposit into Account: " + account.getAccountNumber());
		double amount = Double.parseDouble(scanner.next());
		if(account.deposit(amount)) {
			System.out.println("Deposit successful. New balance: $" + account.getBalance());
		} else {
			System.out.println("Deposit unsuccessful.");
		}
	} 
	
	private void withdrawal(Account account) {
		System.out.println("Please enter the amount you would like to withdraw from Account: " + account.getAccountNumber());
		double amount = Double.parseDouble(scanner.next());
		if(account.withdrawal(amount)) {
			System.out.println("Withdrawal successful. New balance: $" + account.getBalance());
		} else {
			System.out.println("Withdrawal unsuccessful.");
		}
	}
	
	private Account selectAccount(Customer customer) {
		System.out.println("Please select an account.");
		for(int i = 0; i < customer.getAccounts().size();i++) {
			System.out.println("(" + i + ") Account_Id: " + customer.getAccounts().get(i).getAccountNumber());
		}
		
		int index = Integer.parseInt(scanner.next());
		return customer.getAccounts().get(index);
	}
}
